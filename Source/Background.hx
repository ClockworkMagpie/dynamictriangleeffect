/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 *
 * See LICENSE file.
 */

package;

import flash.display.BitmapData;
import flash.display.GradientType;
import flash.display.Shape;
import flash.events.Event;
import flash.geom.Matrix;
import flash.Lib;
import flash.Vector;

class Background extends Shape
{
    // sqrt(3)
    static inline private var SQRT3 = 1.73205080757;

    // distance from center of hexagon to a vertex, in pixels
    static inline private var HEX_SIZE = 24;

    // width of each hexagon in pixels
    static inline private var HEX_WIDTH = HEX_SIZE * 2;

    // height of each hexagon in pixels
    static inline private var HEX_HEIGHT = SQRT3 * 0.5 * HEX_WIDTH;
    
    private var mTexture : BitmapData;
    private var mVertices : Vector<Float>;
    private var mIndices : Vector<Int>;
    private var mUvs : Vector<Float>;
    private var mNoise : Array<Float>;
    private var mNoiseOffset : Int;
    private var mLastUpdate : Int;
    private var mUpdateDelta : Int; ///< time between updates (ms)

    /**
     * @param updateRate Number of times per second to update and redraw.
     */
    public function new(updateRate : Float)
    {
        super();

        // Create the texture. This can be a static image, but it should tile
        // horizontally and be continuous.
        
        var texture = new Shape();
        var mtx = new Matrix();
        // Flash player 11 requires this texture to be at least 2 pixels high
        mtx.createGradientBox(400, 2);
        texture.graphics.beginGradientFill(GradientType.LINEAR,
                                           [0x0000040, 0x000070, 0x000040],
                                           [1, 1, 1],
                                           [0, 128, 255],
                                           mtx);
        texture.graphics.drawRect(0, 0, 400, 2);
        texture.graphics.endFill();
        
        mTexture = new BitmapData(Std.int(texture.width),
                                  Std.int(texture.height),
                                  false);
        mTexture.draw(texture);

        // Create the Perlin noise. Play with the width and the noise parameters
        // for different visual effects.
        //
        // In the OpenFL version I'm using (1.0.5) the HTML5 target doesn't
        // support the perlinNoise method, so if you're building for HTML5
        // you'll have to save this to an image and load it at runtime.
        var noise = new BitmapData(211, 1, false);
        noise.perlinNoise(70, 1, 1, 0, true, true, 7, true);
        mNoiseOffset = 0;

        // Extract one channel of the noise and normalize it to [0,1]
        var noiseMin = 0xFF;
        var noiseMax = 0;
        for(color in noise.getVector(noise.rect))
        {
            var val = color & 0xFF;
            if(val < noiseMin)
            {
                noiseMin = val;
            }
            if(val > noiseMax)
            {
                noiseMax = val;
            }
        }
        mNoise = [];
        for(color in noise.getVector(noise.rect))
        {
            var val = color & 0xFF;
            mNoise.push((val - noiseMin) / (noiseMax - noiseMin));
        }

        cacheAsBitmap = true;

        mUpdateDelta = (updateRate <= 0 ? 1 : Std.int(1000 / updateRate));
        addEventListener(Event.ENTER_FRAME, onFrame);
    }

    /**
     * Set the size of the graphic. This creates the vertex and index arrays
     * used to draw the triangles.
     * @see http://www.redblobgames.com/grids/hexagons/ for the math behind the
     * hexagons
     */
    public function resize(width : Int, height : Int)
    {
        var xhexes = 1 + Math.ceil(width / (0.75 * HEX_WIDTH));
        var yhexes = 1 + Math.ceil(height / HEX_HEIGHT);
        var nhexes = xhexes * yhexes;

        // each hex has six triangles, each triangle has three vertices, each
        // vertex has two coordinates
        mVertices = new Vector<Float>(nhexes * 2 * 3 * 6, true);
        mIndices = new Vector<Int>(nhexes * 3 * 6, true);
        mUvs = new Vector<Float>(nhexes * 2 * 3 * 6, true);

        // the vertex array is just a list of triangles
        for(ii in 0...mIndices.length)
        {
            mIndices[ii] = ii;
        }

        var vidx = 0;
        var iidx = 0;

        // x- and y-coordinates for the vertices of a hexagon centered at the
        // origin.
        var xs = [for(ii in 0...6) HEX_SIZE * Math.cos(ii * 60 * Math.PI / 180)];
        var ys = [for(ii in 0...6) HEX_SIZE * Math.sin(ii * 60 * Math.PI / 180)];
        
        for(ty in 0...yhexes)
        {
            for(tx in 0...xhexes)
            {
                // convert odd-q offset coordinates (tx, ty) to axial
                // coordinates (qq, rr)
                var qq = tx;
                var rr = ty - (tx - (tx & 1)) / 2;

                // convert axial coordinates to pixels to get the center of the
                // hexagon
                var cx = HEX_SIZE * 1.5 * qq;
                var cy = HEX_SIZE * SQRT3 * (rr + 0.5 * qq);

                // set the vertices for each triangle
                for(ii in 0...6)
                {
                    mVertices[vidx++] = cx;
                    mVertices[vidx++] = cy;
                    mVertices[vidx++] = cx + xs[ii];
                    mVertices[vidx++] = cy + ys[ii];
                    mVertices[vidx++] = cx + xs[(ii + 1) % 6];
                    mVertices[vidx++] = cy + ys[(ii + 1) % 6];
                }
            }
        }

        update();
    }

    /**
     * Update the texture coordinates and redraw the triangles.
     */
    public function update()
    {
        mNoiseOffset++;
        fillUVs();
        graphics.clear();
        graphics.beginBitmapFill(mTexture);
        graphics.drawTriangles(mVertices, mIndices, mUvs);
        graphics.endFill();
        mLastUpdate = Lib.getTimer();
    }

    /**
     * Fill the texture coordinates.
     */
    private function fillUVs()
    {
        var uvLength = Std.int(mUvs.length / 6);
        for(ii in 0...uvLength)
        {
            // sample the u coordinate from the Perlin noise
            var u = mNoise[(mNoiseOffset + ii) % mNoise.length];

            // set the same UV for the whole triangle
            for(jj in 0...3)
            {
                // Setting V to 0.5 samples from the middle of the texture
                // vertically.
                // 5233 is a prime number larger than uvLength.
                
                mUvs[6 * ((ii * 5233) % uvLength) + 2 * jj] = u;
                mUvs[6 * ((ii * 5233) % uvLength) + 2 * jj + 1] = 0.5;
            }
        }
    }

    /**
     * Enter frame callback. Update the graphic.
     */
    private function onFrame(event : Event)
    {
        var now = Lib.getTimer();
        if(now - mLastUpdate >= mUpdateDelta)
        {
            update();
        }
    }
}
