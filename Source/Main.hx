/**
 * Copyright 2013 Clockwork Magpie Studios, LLC
 *
 * See LICENSE file.
 */

package;


import flash.display.Sprite;


class Main extends Sprite
{
	
	
	public function new ()
    {
		
		super ();
		
        var background = new Background(20);
        background.resize(stage.stageWidth, stage.stageHeight);
        addChild(background);
		
	}
	
	
}