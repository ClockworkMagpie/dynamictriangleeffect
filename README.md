Dynamic Triangle Background Effect
==========

This is an example of the dynamic triangle effect I used for the background of [HexMatch][]. You can read about the concept and how it works at [Clockwork Magpie Studios][post].

[HexMatch]: http://studios.clockworkmagpie.com/games/hexmatch
[post]: http://studios.clockworkmagpie.com/content/how-make-dynamic-triangle-background-effect
